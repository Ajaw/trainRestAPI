package com.train.rest.Controller;

import com.train.rest.model.DAO.*;
import com.train.rest.model.*;
import com.train.rest.model.dbStations;
import com.train.rest.model.graph.Node;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Dominik on 18.11.2017.
 */
@RestController
public class DisjkstraPathFinder {

    org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DbStationDAO stationDAO;

    @Autowired
    DbConnectedStationsDAO connectedStationsDAO;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    DbArrivalHourDAO arrivalHourDAO;

    @Autowired
    DbConnectionToHoursDAO connectionToHoursDAO;

    @Autowired
    DbConnectionMappingDAO mappingDAO;

    @Autowired
    DbConnectionAvalabitiesDAO avalabitiesDAO;



    private EntityManager session;

    @RequestMapping(value = "/findpath", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String findPath(@RequestParam(name = "statBeg") Integer begin, @RequestParam(name = "statEnd") Integer statEnd) {
        try {


            dbStations stationStart = stationDAO.findById(begin);
            dbStations stationEnd = stationDAO.findById(statEnd);
            session = entityManagerFactory.createEntityManager();
            Date beginDate = new Date();
            List<dbConnectionAvailabilities> availabilities = this.getAvalibbleConnectionsForDay();
            List<dbConnectionToHours> connectionToHoursList = new LinkedList<>();
            HashMap<Node, HashMap<Integer, ArrivalHours>> graph = new LinkedHashMap<>();
            HashMap<Node, HashMap<Integer, ArrivalHours>> finalGraph = new LinkedHashMap<>();
            for (dbConnectionAvailabilities availability : availabilities) {
                dbConnectionToHours connectionToHours = connectionToHoursDAO.findById(availability.getMapping().getConnectionToHourId());
                connectionToHoursList.add(connectionToHours);

                for (ArrivalHours arrivalHours : connectionToHours.getConHoursList()) {
                    HashMap<Integer, ArrivalHours> arrivalHoursList =this.getENtryFromGraph(graph,arrivalHours.getStationOnConnectionId());
                    boolean wasNotPResent = false;
                    if (arrivalHoursList == null) {
                        wasNotPResent = true;
                        arrivalHoursList = new LinkedHashMap<>();
                    }
                    arrivalHoursList.putIfAbsent(arrivalHours.getId(), arrivalHours);
                    if (wasNotPResent) {
                        Node node = new Node();
                        node.setPathValue((double) Integer.MAX_VALUE);
                        node.setStationId(arrivalHours.getStationOnConnectionId());
                        graph.put(node, arrivalHoursList);
                    }
                    arrivalHours.getHour();
                }
            }
            List<Node> list = new ArrayList<Node>();
            for (Map.Entry<Node, HashMap<Integer, ArrivalHours>> integerArrivalHoursHashMap : graph.entrySet()) {
               list.add(integerArrivalHoursHashMap.getKey());
            }
            boolean first =true;

            Node currentNode = this.getNodeFromGraph(graph,begin);
            currentNode.setPathValue(0.0);
            while (!list.isEmpty()) {
                try {
                    Node node = getMin(list);

                    logger.info(stationDAO.findById(node.getStationId()).getStationName() + "Id: " + node.getStationId());
                    Node finalNode = node;
                    list.removeIf(a -> a.getStationId().equals(finalNode.getStationId()));
                    if (node==null){
                      //  graph.remove(currentNode);
                        logger.info("STOP");
                       // currentNode = begin;
                        node = getMin(list);
                    }
                    if (node.getStationId()==statEnd) {
                        logger.info("found stat end");
                       break;
                    }
                    //currentNode = this.getNodeFromGraph(graph,node.getConnectedStations().getMotherStationId());


                    ArrivalHours currentNeighbour = null;
                    if (node.getStationId()==318){
                        logger.info("found piotrowice");
                    }
                    for (Node neighbor : getGraphNodeChildren(node,graph)) {
                        if (node.getPathValue().equals(Integer.MAX_VALUE)){
                            logger.error("problem with value");
                        }

                        ArrivalHours arrivalHours = null;
                        try {
                            logger.info(stationDAO.findById(neighbor.getStationId()).getStationName());
                            if (node.getArrivalHour()!=null)
                                arrivalHours = this.getConnection(node.getStationId(),graph,neighbor.getStationId(),node);
                            else {
                               if (first) {
                                    Node tempNode =new Node();

                                    tempNode.setPathValue(0.0);
                                    ArrivalHours arrivalHours1 =new ArrivalHours();
                                    Date date = Date.from(LocalDateTime.of(2018,5,10,10,20).atZone(ZoneId.systemDefault()).toInstant());
                                    arrivalHours1.setDepartureTime(new Timestamp(date.getTime()));
                                    tempNode.setArrivalHour(arrivalHours1);

                                    arrivalHours = this.getConnection(node.getStationId(), graph, neighbor.getStationId(), tempNode);
                               }
                                else {

                                  // continue;
                                }
                            }

                        }
                        catch (Exception ex){
                            logger.error(ex.getMessage());
                        }
                        if (Objects.equals(neighbor.getStationId(), statEnd)){
                            logger.error("Found stat end");

                        }
                        if (arrivalHours!=null) {
                            double cost =  arrivalHours.getHour();
                            logger.info(String.valueOf(cost));
                            double d = cost;
                            if (d < neighbor.getPathValue()) {
                                if (d<0){
                                    logger.error("d is minus");
                                }
                                //list.remove(node);
                                // currentNeighbour = neighbor;
                               // node.setPathValue(d);
                              //  node.setArrivalHour(arrivalHours);
                                Node finalNode1 = neighbor;
                                Optional<Node> actNode = list.stream().filter(a -> a.getStationId().equals(finalNode1.getStationId())).findFirst();
                                if (actNode.isPresent()) {
                                    actNode.get().setPathValue(d);
                                    actNode.get().setArrivalHour(arrivalHours);
                                    neighbor.setArrivalHour(arrivalHours);
                                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
                                    logger.info(dateTimeFormatter.format(arrivalHours.getDepartureTime().toLocalDateTime()));
                                   // neighbor.setArrivalHour(arrivalHours);
                                    neighbor.setPathValue(d);
                                    logger.info("Updated Node content");
                                    logger.info("------");
                                }

                            }
                        }
                    }
                    finalGraph.put(node,graph.get(node));
                    graph.remove(this.getNodeFromGraph(graph,node.getStationId()));
                    first=false;
                    if (Objects.equals(node.getStationId(), statEnd)){
                        logger.error("Found stat end");
                        break;
                    }
                   // graph.remove(node);

                    // currentNode = this.getNodeFromGraph(graph, currentNeighbour.getConnectedStations().getMotherStationId());




                    List<dbConnectedStations> connectedStations = connectedStationsDAO.findByMotherStationId(stationStart.getId());

                   // logger.info(String.valueOf(availabilities.size()));

                }
                catch (Exception ex){
                    logger.error(ex.getMessage()
                    );
                   // ex.printStackTrace();
                }
            }
            Integer currentStat = statEnd;
             DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
            do {
                Node actualNode = this.getNodeFromGraph(finalGraph,currentStat);
                logger.info(stationDAO.findById(actualNode.getStationId()).getStationName());
                logger.info(String.valueOf(actualNode.getArrivalHour().getConnectionToHourId()));
                LocalDateTime depTime = actualNode.getArrivalHour().getDepartureTime().toLocalDateTime();
                logger.info("Departure Time:" + dateTimeFormatter.format(depTime));
                logger.info("---------------");
                currentStat = actualNode.getArrivalHour().getConnectedStations().getMotherStationId();
                if (actualNode.getStationId()==366){
                    logger.info("found kobior");
                }
            }while (currentStat!=begin);

            logger.info("Finish");
        }
        catch(Exception ex){
                logger.error(ex.getMessage());
            }
            return "ok";


    }

    private HashMap<Integer, ArrivalHours> getENtryFromGraph(HashMap<Node, HashMap<Integer, ArrivalHours>> graph, Integer stationId){
        for (Map.Entry<Node, HashMap<Integer, ArrivalHours>> nodeHashMapEntry : graph.entrySet()) {
            if (nodeHashMapEntry.getKey().getStationId().equals(stationId))
                return nodeHashMapEntry.getValue();
        }
        return null;
    }

    private Node getNodeFromGraph( HashMap<Node, HashMap<Integer, ArrivalHours>> graph,Integer stationId){
        for (Map.Entry<Node, HashMap<Integer, ArrivalHours>> nodeHashMapEntry : graph.entrySet()) {
            if (nodeHashMapEntry.getKey().getStationId().equals(stationId))
                return nodeHashMapEntry.getKey();
        }
        return null;
    }

    private double getCost(ArrivalHours node , ArrivalHours neighbour){
        return neighbour.getHour() - node.getHour();
    }

    private List<ArrivalHours> getGraphChildren(Integer node,HashMap<Node, HashMap<Integer, ArrivalHours>> graph ){
        for (Map.Entry<Node, HashMap<Integer, ArrivalHours>> nodeHashMapEntry : graph.entrySet()) {
            if (nodeHashMapEntry.getKey().getStationId().equals(node))
                return new LinkedList<>(nodeHashMapEntry.getValue().values());
        }
        return null;
    }

    private List<Node> getGraphNodeChildren(Node node,HashMap<Node, HashMap<Integer, ArrivalHours>> graph ){
        List<ArrivalHours> arrivalHours = new LinkedList<>();
        for (Map.Entry<Node, HashMap<Integer, ArrivalHours>> nodeHashMapEntry : graph.entrySet()) {
            if (nodeHashMapEntry.getKey().getStationId().equals(node.getStationId())) {
                arrivalHours = new LinkedList<>(nodeHashMapEntry.getValue().values());
            }
        }
        HashSet<Integer> connectedNode = new LinkedHashSet<>();

        for (ArrivalHours arrivalHour : arrivalHours) {
            connectedNode.add(arrivalHour.getConnectedStations().getConnectedStationId());
        }
        List<Node> nodeList = new LinkedList<>();
        for (Integer integer : connectedNode) {
            Optional<Node> testNode = graph.keySet().stream().filter(a -> a.getStationId().equals(integer)).findFirst();
            if (testNode.isPresent()){
                nodeList.add(testNode.get());
            }
        }

        return nodeList;
    }

    public Node getMin(List<Node> realGraph){
       // return realGraph.keySet().stream().sorted(((a,a1) -> Integer.compare(a.getPathValue(),a1.getPathValue()))).findFirst().get();

        List<Node> testList = realGraph.stream().sorted(((a,a1) -> Double.compare(a.getPathValue(),a1.getPathValue()))).collect(Collectors.toList());
        if (testList.get(0).getPathValue().equals(100000)){
            logger.error("problem with value");
        }
        return testList.get(0);
    }


        public ArrivalHours getConnection(Integer node,HashMap<Node, HashMap<Integer, ArrivalHours>> realGraph,Integer toConnectTo,Node previousTime) {
        ArrivalHours toRet = null;
        try {
            HashMap<Node, HashMap<Integer, ArrivalHours>> graph = new HashMap<>(realGraph);
            for (ArrivalHours arrivalHours : this.getENtryFromGraph(graph,node).values()) {

                if (arrivalHours.getConnectedStations().getConnectedStationId()!=0) {
                    for (ArrivalHours hours : this.getENtryFromGraph(graph,arrivalHours.getConnectedStations().getMotherStationId()).values()) {
                        Double timeValue  = 0.0;
                        if (previousTime.getArrivalHour()!=null) {
                            timeValue = previousTime.getArrivalHour().getHour();
                        }
                        if (hours.getConnectedStations().getConnectedStationId()==toConnectTo && hours.getHour()>= timeValue) {
                            if (toRet==null)
                                toRet = hours;
                           // if (arrivalHourss.stream().filter(a -> a.getId()==hours.getId()).findFirst().isPresent()) {
                                if (toRet.getHour()>=hours.getHour()) {
                                    // realGraph.get(node).values().remove(hours);
                                    toRet = hours;

                                }
                           // }
                        }
                    }
                }
            }
        }catch (Exception ex){
            logger.info(ex.getMessage());
           // ex.printStackTrace();
            //return new LinkedList<ArrivalHours>(graph.get(419).values()).get(0);
        }

      // realGraph.remove(this.getNodeFromGraph(realGraph,node));
          //realGraph.get(node).values().remove(toRet);
           ArrivalHours finalToRet = toRet;
          //  realGraph.remove(node);
            if (toRet==null)
                return toRet;
     //         arrivalHourss.removeIf(a -> a.getConnectedStations().getMotherStationId() == finalToRet.getConnectedStations().getMotherStationId()
       //     && a.getConnectedStations().getConnectedStationId() == finalToRet.getConnectedStations().getConnectedStationId());

            return toRet;
        }

    @RequestMapping(value = "/deletetrash", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String deleteTrash() {
        List<dbConnectionToHours> dbConnectionToHours = connectionToHoursDAO.findAll();
        boolean delete = false;
        for (com.train.rest.model.dbConnectionToHours dbConnectionToHour : dbConnectionToHours) {
            try {
                ArrivalHours arrivalHour = new LinkedList<>(dbConnectionToHour.getConHoursList()).get(0);

                if (1 == arrivalHour.getDepartureTime().toLocalDateTime().getYear() && arrivalHour.getPlatform() == null) {
                    delete = true;
                    List<dbConnectionMappings> connectionMappings = mappingDAO.findByConnectionToHourId(dbConnectionToHour.getId());
                    mappingDAO.delete(connectionMappings);
                    List<dbConnectionAvailabilities> availabilities = avalabitiesDAO.findByMapping_ConnectionToHourId(dbConnectionToHour.getId());
                    avalabitiesDAO.delete(availabilities);
                    arrivalHourDAO.delete(dbConnectionToHour.getConHoursList());
                   connectionToHoursDAO.delete(dbConnectionToHour);
                }
            }
            catch (Exception ex){
                logger.error(ex.getMessage());
            }

        }
        return "ok";

    }
    @RequestMapping(value = "/mapconnstatohour", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String mapConnHourToSta(){

        List<dbConnectionToHours> connectionToHours = connectionToHoursDAO.findAll();

        for (dbConnectionToHours connectionToHour : connectionToHours){
            List<dbConnectedStations> connectedStations = new LinkedList<>(connectionToHour.getConnection().getConnectedStationsList());
             List<ArrivalHours> arrivalHours = new LinkedList<>(connectionToHour.getConHoursList());
             int current =0;
             for (ArrivalHours arrivalHour : arrivalHours){
                 if (arrivalHour.getConnectedStations()==null) {
                     try {
                         dbConnectedStations connectedStation = connectedStations.get(current);
                         arrivalHour.setConnectedStations(connectedStation);
                         arrivalHourDAO.save(arrivalHour);
                         current++;
                     } catch (Exception ex) {
                         logger.error(ex.getMessage());
                     }
                 }
             }

        }


        return "ok";

    }

    private List<dbConnectionAvailabilities> getAvalibbleConnectionsForDay(){

        List<dbConnectionAvailabilities> mappings =  session.createNativeQuery("" +
                "SELECT ava.* FROM train_db.dbConnectionAvailabilities ava\n" +
                "left join dbConnectionMappings map on map.id=ava.mapping_id\n" +
                "where (map.monday=true or ignoreWeekDays=true)  and ('2018-06-04' between ava.beggingDate and ava.endDate)\n"
                , dbConnectionAvailabilities.class)
                .getResultList();
        return mappings;

    }



}
