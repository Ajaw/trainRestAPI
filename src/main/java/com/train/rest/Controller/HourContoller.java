package com.train.rest.Controller;

import com.train.rest.model.DAO.*;
import com.train.rest.model.dbConnectionToHours;
import com.train.rest.model.dbConnections;
import com.train.rest.model.dbStations;
import com.train.rest.model.reps.connectionToHoursRepository;
import com.train.rest.model.reps.connectionRepository;
import com.train.rest.model.reps.*;
import com.train.rest.model.*;
import com.train.rest.model.precomputedData.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 08.03.2017.
 */

@RestController
public class HourContoller {



    @RequestMapping(value = "/getdata", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public connectionToHoursRepository getAllData(@RequestParam("version_id") Integer versionId){

        try {
          List<dbConnectionToHours> tempSet =   hoursDAO.findByVersionId(versionId);
          connectionToHoursRepository rep = new connectionToHoursRepository();
          rep.connectionToHours = tempSet;
          String response ="";
          return rep;
          /**  for (dbConnectionToHours hour: tempSet
                 ) {
                 response += hour.getId() + " ";

            }**/
           // return  response;
        }
        catch (Exception ex){
            return null;
        }


    }

    @RequestMapping(value = "/getconnections", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public connectionRepository getAllConnections(){
        try {
            connectionRepository rep = new connectionRepository();
            List<dbConnections> tempSet = connectionDAO.findAll();
            rep.connectionRep = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }


    @RequestMapping(value = "/getstations", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public stationRepository getAllStations(){
        try {
            stationRepository rep = new stationRepository();
            List<dbStations> tempSet = stationDAO.findAll();
            rep.stationsRep = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }


    @RequestMapping(value = "/getstationsonconnection", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public stationOnConnectionRepository getAllStationOnConnecton(){
        try {
            stationOnConnectionRepository rep = new stationOnConnectionRepository();
            List<dbStationsOnConnections> tempSet = stationOnConnectionDAO.findAll();
            rep.stationsOnConnectionsRep = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }


    @RequestMapping(value = "/getarrivalhours", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public stationOnConnectionRepository getArrivalHours(){
        try {
            stationOnConnectionRepository rep = new stationOnConnectionRepository();
            List<dbStationsOnConnections> tempSet = stationOnConnectionDAO.findAll();
            rep.stationsOnConnectionsRep = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }

    @RequestMapping(value = "/getprecomputedparts", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public precomputedpartsRepository getParts(){
        try {
            precomputedpartsRepository rep = new precomputedpartsRepository();
            List<dbPrecomputedPart> tempSet = partsDAO.findAll();
            rep.precomputedParts = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }

    @RequestMapping(value = "/getmappings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public mappingRepository getMappings(@RequestParam("version_id") Integer versionId){
        try {
            mappingRepository rep = new mappingRepository();
            List<dbConnectionMappings> tempSet = mappingDAO.findByVersionId(versionId);
            rep.mappings = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }

    @RequestMapping(value = "/getversions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public versionRepository getVestions(@RequestParam("lastdate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date lastdate){
        try {
            versionRepository rep = new versionRepository();
            List<dbScheduleVersions> tempSet = scheduleVersionDAO.findByDateToAfter(lastdate);
            rep.versionsRep = tempSet;
            return rep;
        }
        catch (Exception ex){
            return null;
        }
    }

    @Autowired
    private DbConnectionMappingDAO mappingDAO;

    @Autowired
    private DbPrecomputedPartsDAO partsDAO;

    @Autowired
    private DbStationOnConnectionDAO stationOnConnectionDAO;

    @Autowired
    private DbStationDAO stationDAO;


    @Autowired
    private DbConnectionDAO connectionDAO;

    @Autowired
    private DbConnectionToHoursDAO hoursDAO;

    @Autowired
    private DbScheduleVersionDAO scheduleVersionDAO;
}
