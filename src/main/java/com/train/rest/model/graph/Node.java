package com.train.rest.model.graph;

import com.train.rest.model.ArrivalHours;

/**
 * Created by Dominik on 31.05.2018.
 */
public class Node {

    private Double pathValue;

    public ArrivalHours getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(ArrivalHours arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    private ArrivalHours arrivalHour;

    public Double getPathValue() {
        return pathValue;
    }

    public void setPathValue(Double pathValue) {
        this.pathValue = pathValue;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    private Integer stationId;

}
