package com.train.rest.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Dominik on 18.11.2017.
 */
@Entity
public class dbConnectedStations {
    private int id;
    private int motherStationId;
    private int connectedStationId;
    private int previousStationId;
    private int connectionId;
    private byte finalStation;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "motherStationID")
    public int getMotherStationId() {
        return motherStationId;
    }

    public void setMotherStationId(int motherStationId) {
        this.motherStationId = motherStationId;
    }

    @Basic
    @Column(name = "connectedStationID")
    public int getConnectedStationId() {
        return connectedStationId;
    }

    public void setConnectedStationId(int connectedStationId) {
        this.connectedStationId = connectedStationId;
    }

    @Basic
    @Column(name = "previousStationID")
    public int getPreviousStationId() {
        return previousStationId;
    }

    public void setPreviousStationId(int previousStationId) {
        this.previousStationId = previousStationId;
    }

    @Basic
    @Column(name = "connectionID")
    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    @Basic
    @Column(name = "finalStation")
    public byte getFinalStation() {
        return finalStation;
    }

    public void setFinalStation(byte finalStation) {
        this.finalStation = finalStation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectedStations that = (dbConnectedStations) o;

        if (id != that.id) return false;
        if (motherStationId != that.motherStationId) return false;
        if (connectedStationId != that.connectedStationId) return false;
        if (previousStationId != that.previousStationId) return false;
        if (connectionId != that.connectionId) return false;
        if (finalStation != that.finalStation) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + motherStationId;
        result = 31 * result + connectedStationId;
        result = 31 * result + previousStationId;
        result = 31 * result + connectionId;
        result = 31 * result + (int) finalStation;
        return result;
    }
}
