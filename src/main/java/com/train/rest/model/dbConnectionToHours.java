package com.train.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 08.03.2017.
 */
@Entity
public class dbConnectionToHours {
    private int id;
    private int connectionId;
    private dbConnections connection;
    private boolean onWeekendConnection;
    private Collection<ArrivalHours> conHoursList;
    private Integer versionId;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "connectionId")
    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    @Basic
    @Column(name = "onWeekendConnection")
    public boolean getOnWeekendConnection() {
        return onWeekendConnection;
    }

    public void setOnWeekendConnection(boolean onWeekendConnection) {
        this.onWeekendConnection = onWeekendConnection;
    }

    @Basic
    @Column(name = "version_id")
    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(updatable = false,insertable = false,name = "connectionId",referencedColumnName = "id")
    public dbConnections getConnection() {
        return connection;
    }

    public void setConnection(dbConnections connection) {
        this.connection = connection;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectionToHours that = (dbConnectionToHours) o;

        if (id != that.id) return false;
        if (connectionId != that.connectionId) return false;
        if (onWeekendConnection != that.onWeekendConnection) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + connectionId;

        return result;
    }

    @OneToMany(mappedBy = "connectionToHourId")
    public Collection<ArrivalHours> getConHoursList() {
        return conHoursList;
    }

    public void setConHoursList(Collection<ArrivalHours> conHoursList) {
        this.conHoursList = conHoursList;
    }
}
