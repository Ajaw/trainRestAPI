package com.train.rest.model.DAO;

import com.train.rest.model.dbConnectionAvailabilities;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 12.11.2017.
 */
public interface DbConnectionAvalabitiesDAO extends CrudRepository<dbConnectionAvailabilities,String> {

    public List<dbConnectionAvailabilities> findAll();

    public List<dbConnectionAvailabilities> findByMapping_ConnectionToHourId(Integer mappingConnectionToHourID);

}
