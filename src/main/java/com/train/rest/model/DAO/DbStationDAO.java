package com.train.rest.model.DAO;

import com.train.rest.model.dbConnections;
import com.train.rest.model.dbStations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 31.03.2017.
 */
@Repository
public interface DbStationDAO extends CrudRepository<dbStations,String> {

    public List<dbStations> findAll();

    public dbStations findById(Integer id);

}
