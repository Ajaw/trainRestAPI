package com.train.rest.model.DAO;

import com.train.rest.model.precomputedData.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
public interface DbPrecomputedConnectionsDAO  extends CrudRepository<dbPrecomputedConnection,String> {

    public List<dbPrecomputedConnection> findAll();


}
