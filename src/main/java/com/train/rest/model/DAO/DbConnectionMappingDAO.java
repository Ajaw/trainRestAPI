package com.train.rest.model.DAO;

import com.train.rest.model.dbConnectionMappings;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 12.11.2017.
 */
public interface DbConnectionMappingDAO extends CrudRepository<dbConnectionMappings,String> {

    public List<dbConnectionMappings> findAll();

    public List<dbConnectionMappings> findByVersionId(Integer versionId);

    public List<dbConnectionMappings> findByConnectionToHourId(Integer connectionToHourId);

}
