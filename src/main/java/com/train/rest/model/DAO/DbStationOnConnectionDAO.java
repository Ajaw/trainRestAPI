package com.train.rest.model.DAO;

import com.train.rest.model.dbStations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.train.rest.model.dbStationsOnConnections;

import java.util.List;

/**
 * Created by Dominik on 01.04.2017.
 */
@Repository
public interface DbStationOnConnectionDAO extends CrudRepository<dbStationsOnConnections,String> {

    public List<dbStationsOnConnections> findAll();

}
