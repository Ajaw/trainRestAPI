package com.train.rest.model.DAO;

import com.train.rest.model.dbScheduleVersions;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 03.12.2017.
 */
public interface DbScheduleVersionDAO extends CrudRepository<dbScheduleVersions,String> {

    public List<dbScheduleVersions> findAll();

    public List<dbScheduleVersions> findByDateToAfter(Date toDate);

}
