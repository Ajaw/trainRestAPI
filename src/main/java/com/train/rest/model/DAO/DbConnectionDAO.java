package com.train.rest.model.DAO;

import com.train.rest.model.dbConnections;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 26.03.2017.
 */
@Repository
public interface DbConnectionDAO extends CrudRepository<dbConnections,String> {

    public List<dbConnections> findAll();

}
