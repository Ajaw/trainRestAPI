package com.train.rest.model.DAO;

import com.train.rest.model.ArrivalHours;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 18.11.2017.
 */
public interface DbArrivalHourDAO extends CrudRepository<ArrivalHours,String> {

    public List<ArrivalHours> findAll();

}
