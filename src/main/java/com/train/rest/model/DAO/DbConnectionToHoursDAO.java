package com.train.rest.model.DAO;


import com.train.rest.model.dbConnectionToHours;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Created by Dominik on 08.03.2017.
 */
@Repository
public interface DbConnectionToHoursDAO extends CrudRepository<dbConnectionToHours,String> {

    public List<dbConnectionToHours> findAll();

    public dbConnectionToHours findById(int id);

    public List<dbConnectionToHours> findByVersionId(Integer versionId);

}
