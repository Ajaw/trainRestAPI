package com.train.rest.model.DAO;

import com.train.rest.model.dbConnectedStations;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 18.11.2017.
 */
public interface DbConnectedStationsDAO extends CrudRepository<dbConnectedStations,String> {

    public List<dbConnectedStations> findAll();

    public dbConnectedStations findById(Integer id);

    public List<dbConnectedStations> findByMotherStationId(Integer motherStationId);

}
