package com.train.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Dominik on 12.11.2017.
 */
@Entity
@Table(name = "dbConnectionAvailabilities")
public class dbConnectionAvailabilities {
    private int id;
    private Date beggingDate;
    private Date endDate;
    private boolean ignoreWeekDays;
    private dbConnectionMappings mapping;


    private Integer mapping_id;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "beggingDate")
    public Date getBeggingDate() {
        return beggingDate;
    }

    public void setBeggingDate(Date beggingDate) {
        this.beggingDate = beggingDate;
    }

    @Basic
    @Column(name = "endDate")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "ignoreWeekDays")
    public boolean getIgnoreWeekDays() {
        return ignoreWeekDays;
    }

    public void setIgnoreWeekDays(boolean ignoreWeekDays) {
        this.ignoreWeekDays = ignoreWeekDays;
    }

    @Basic
    @Column(name = "mapping_id")
    public Integer getMapping_id() {
        return mapping_id;
    }

    public void setMapping_id(Integer mapping_id) {
        this.mapping_id = mapping_id;
    }


    @ManyToOne
    @JoinColumn(name = "mapping_id",insertable = false,updatable = false)
    @JsonIgnore
    public dbConnectionMappings getMapping() {
        return mapping;
    }

    public void setMapping(dbConnectionMappings mapping) {
        this.mapping = mapping;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectionAvailabilities that = (dbConnectionAvailabilities) o;

        if (id != that.id) return false;
        if (ignoreWeekDays != that.ignoreWeekDays) return false;
        if (beggingDate != null ? !beggingDate.equals(that.beggingDate) : that.beggingDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (beggingDate != null ? beggingDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);

        return result;
    }
}
