package com.train.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 08.03.2017.
 */
@Entity
public class dbConnections {
    private int id;
    private Collection<dbConnectionToHours> hoursList;
    private Collection<dbStationsOnConnections> stationsList;
    private Collection<dbConnectedStations> connectedStationsList;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnections that = (dbConnections) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @OneToMany(targetEntity = dbConnectionToHours.class ,mappedBy = "connectionId")
    public Collection<dbConnectionToHours> getHoursList() {
        return hoursList;
    }

    public void setHoursList(Collection<dbConnectionToHours> hoursList) {
        this.hoursList = hoursList;
    }

    @OneToMany(targetEntity = dbStationsOnConnections.class ,mappedBy = "connectionId")
    public Collection<dbStationsOnConnections> getStationsList() {
        return stationsList;
    }

    public void setStationsList(Collection<dbStationsOnConnections> stationsList) {
        this.stationsList = stationsList;
    }

    @JsonIgnore
    @OneToMany(targetEntity = dbConnectedStations.class ,mappedBy = "connectionId")
    public Collection<dbConnectedStations> getConnectedStationsList() {
        return connectedStationsList;
    }

    public void setConnectedStationsList(Collection<dbConnectedStations> connectedStationsList) {
        this.connectedStationsList = connectedStationsList;
    }

}
