package com.train.rest.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Dominik on 08.03.2017.
 */
@Entity
public class ArrivalHours {
    private int id;
    private int connectionToHourId;
    private int stationOnConnectionId;
    private Timestamp arrivalTime;
    private Timestamp departureTime;
    private String platform;
    private String track;
    private dbConnectedStations connectedStations;

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    private Integer hour;

    @Transient
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HHmm");

    @Transient
    public Double getHour() {
        Double number = 1.0;
        if (hour==null){
             LocalDateTime localDateTime =  departureTime.toLocalDateTime();
            // System.out.println(dateTimeFormatter.format(localDateTime));
             number = Double.parseDouble("0.1"+dateTimeFormatter.format(localDateTime)+"1");
        }
        return number;
    }

    @Transient
    public Double getHourWithConnectionSwitch(ArrivalHours arrivalHours) {
        Double number = 1.0;
        if (hour==null){
            LocalDateTime localDateTime =  departureTime.toLocalDateTime();

            if (arrivalHours!=null && arrivalHours.getConnectionToHourId()!=this.getConnectionToHourId()){
                localDateTime = localDateTime.plusMinutes(25);
            }
            //System.out.println(dateTimeFormatter.format(localDateTime));
            number = Double.parseDouble("0.1"+dateTimeFormatter.format(localDateTime)+"1");
        }

        return number;
    }

    @Transient
    public Integer getArrivalHour() {
        if (hour==null){
            LocalDateTime localDateTime =  arrivalTime.toLocalDateTime();
            System.out.println(dateTimeFormatter.format(localDateTime));
            hour = Integer.parseInt("1"+dateTimeFormatter.format(localDateTime)+"1");
        }
        return hour;
    }

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "newStationID")
    public int getStationOnConnectionId() {
        return stationOnConnectionId;
    }

    public void setStationOnConnectionId(int stationOnConnectionId) {
        this.stationOnConnectionId = stationOnConnectionId;
    }

    @Basic
    @Column(name = "connectionToHourId")
    public int getConnectionToHourId() {
        return connectionToHourId;
    }

    public void setConnectionToHourId(int connectionToHourId) {
        this.connectionToHourId = connectionToHourId;
    }

    @Basic
    @Column(name = "arrivalTime")

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Basic
    @Column(name = "departureTime")
    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    @Basic
    @Column(name = "platform")
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Basic
    @Column(name = "track")
    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }


    @ManyToOne
    @JoinColumn(name = "connectedStation_id",referencedColumnName = "id")
    public dbConnectedStations getConnectedStations() {
        return connectedStations;
    }

    public void setConnectedStations(dbConnectedStations connectedStations) {
        this.connectedStations = connectedStations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalHours that = (ArrivalHours) o;

        if (id != that.id) return false;
        if (connectionToHourId != that.connectionToHourId) return false;
        if (arrivalTime != null ? !arrivalTime.equals(that.arrivalTime) : that.arrivalTime != null) return false;
        if (departureTime != null ? !departureTime.equals(that.departureTime) : that.departureTime != null)
            return false;
        if (platform != null ? !platform.equals(that.platform) : that.platform != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + connectionToHourId;
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        return result;
    }


}
