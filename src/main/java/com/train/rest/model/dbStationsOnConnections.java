package com.train.rest.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Dominik on 08.03.2017.
 */
@Entity
public class dbStationsOnConnections {
    private int id;
    private int connectionId;
    private int stationId;
    private int stationNumberOnRoute;
    private double distanceFromLastStation;

    @Id
    @Column(name = "Id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "connectionId")
    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    @Basic
    @Column(name = "stationID")
    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    @Basic
    @Column(name = "stationNumberOnRoute")
    public int getStationNumberOnRoute() {
        return stationNumberOnRoute;
    }

    public void setStationNumberOnRoute(int stationNumberOnRoute) {
        this.stationNumberOnRoute = stationNumberOnRoute;
    }

    @Basic
    @Column(name = "distanceFromLastStation")
    public double getDistanceFromLastStation() {
        return distanceFromLastStation;
    }

    public void setDistanceFromLastStation(double distanceFromLastStation) {
        this.distanceFromLastStation = distanceFromLastStation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbStationsOnConnections that = (dbStationsOnConnections) o;

        if (id != that.id) return false;
        if (connectionId != that.connectionId) return false;
        if (stationId != that.stationId) return false;
        if (stationNumberOnRoute != that.stationNumberOnRoute) return false;
        if (Double.compare(that.distanceFromLastStation, distanceFromLastStation) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + connectionId;
        result = 31 * result + stationId;
        result = 31 * result + stationNumberOnRoute;
        temp = Double.doubleToLongBits(distanceFromLastStation);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
