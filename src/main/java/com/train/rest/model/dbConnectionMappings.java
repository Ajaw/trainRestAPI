package com.train.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 12.11.2017.
 */
@Entity
public class dbConnectionMappings {
    private int id;
    private boolean monday;
    private boolean tuesday;
    private boolean wenesday;
    private boolean thursday;
    private boolean friday;
    private boolean saturday;
    private boolean sunday;
    private String connectionNumber;
    private boolean wasProcessed;
    private Integer connectionToHourId;


    private Integer versionId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "monday")
    public boolean getMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    @Basic
    @Column(name = "tuesday")
    public boolean getTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    @Basic
    @Column(name = "wenesday")
    public boolean getWenesday() {
        return wenesday;
    }

    public void setWenesday(boolean wenesday) {
        this.wenesday = wenesday;
    }

    @Basic
    @Column(name = "thursday")
    public boolean getThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    @Basic
    @Column(name = "friday")
    public boolean getFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    @Basic
    @Column(name = "saturday")
    public boolean getSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    @Basic
    @Column(name = "sunday")
    public boolean getSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    @Basic
    @Column(name = "connection_number")
    public String getConnectionNumber() {
        return connectionNumber;
    }

    public void setConnectionNumber(String connectionNumber) {
        this.connectionNumber = connectionNumber;
    }

    @Basic
    @Column(name = "was_processed")
    public boolean getWasProcessed() {
        return wasProcessed;
    }

    public void setWasProcessed(boolean wasProcessed) {
        this.wasProcessed = wasProcessed;
    }

    private Collection<dbConnectionAvailabilities> availabilities;

    @Basic
    @Column(name = "version_id")
    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }



    @OneToMany(mappedBy = "mapping")
    public Collection<dbConnectionAvailabilities> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(Collection<dbConnectionAvailabilities> availabilities) {
        this.availabilities = availabilities;
    }

    @Basic
    @Column(name = "connection_ID")
    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    private Integer connectionId;

    @Basic
    @Column(name = "connectionToHour_ID")
    public Integer getConnectionToHourId() {
        return connectionToHourId;
    }

    public void setConnectionToHourId(Integer connectionToHourId) {
        this.connectionToHourId = connectionToHourId;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectionMappings that = (dbConnectionMappings) o;

        if (id != that.id) return false;
        if (monday != that.monday) return false;
        if (tuesday != that.tuesday) return false;
        if (wenesday != that.wenesday) return false;
        if (thursday != that.thursday) return false;
        if (friday != that.friday) return false;
        if (saturday != that.saturday) return false;
        if (sunday != that.sunday) return false;
        if (wasProcessed != that.wasProcessed) return false;
        if (connectionNumber != null ? !connectionNumber.equals(that.connectionNumber) : that.connectionNumber != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;

        return result;
    }
}
