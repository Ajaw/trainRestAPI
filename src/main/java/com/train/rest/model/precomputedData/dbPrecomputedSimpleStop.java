package com.train.rest.model.precomputedData;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedSimpleStops")
public class dbPrecomputedSimpleStop {
    private int id;
    private int station_ID;
    private dbPrecomputedPart part;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbPrecomputedSimpleStop that = (dbPrecomputedSimpleStop) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "precomputedPartID")
    @JsonBackReference
    public dbPrecomputedPart getPart() {
        return null;
    }

    public void setPart(dbPrecomputedPart partID) {
        this.part = partID;
    }



    @Column(name = "stationID")
    public int getStation_ID() {
        return station_ID;
    }

    public void setStation_ID(int station_ID) {
        this.station_ID = station_ID;
    }
}
