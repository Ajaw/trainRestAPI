package com.train.rest.model.precomputedData;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedParts")
public class dbPrecomputedPart {

    private int id;
    private Set<dbPrecomputedConnection> connections;
    private Set<dbPrecomputedSimpleStop> stopsOnStation;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbPrecomputedPart that = (dbPrecomputedPart) o;

        if (id != that.id) return false;

        return true;
    }

    @OneToMany(targetEntity = dbPrecomputedConnection.class,mappedBy = "part")
    public Set<dbPrecomputedConnection> getConnections() {
        return connections;
    }

    public void setConnections(Set<dbPrecomputedConnection> connections) {
        this.connections = connections;
    }


    @OneToMany(targetEntity = dbPrecomputedSimpleStop.class,mappedBy = "part")
    public Set<dbPrecomputedSimpleStop> getStopsOnStation() {
        return stopsOnStation;
    }

    public void setStopsOnStation(Set<dbPrecomputedSimpleStop> stopsOnStation) {
        this.stopsOnStation = stopsOnStation;
    }





    @Override
    public int hashCode() {
        return id;
    }
}
