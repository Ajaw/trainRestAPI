package com.train.rest.model.precomputedData;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedStops")
public class dbPrecomputedStop {

    private int id;
    private int connectedStationID;
    private dbPrecomputedConnection dbPrecomputedConnectionID;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbPrecomputedStop that = (dbPrecomputedStop) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "precomputedConnectionID")
    @JsonBackReference
    public dbPrecomputedConnection getDbPrecomputedConnectionID() {
        return null;
    }

    public void setDbPrecomputedConnectionID(dbPrecomputedConnection dbPrecomputedConnectionID) {
        this.dbPrecomputedConnectionID = dbPrecomputedConnectionID;    }



    @Column(name = "connectedStationID")
    public int getConnectedStationID() {
        return connectedStationID;
    }

    public void setConnectedStationID(int connectedStationID) {
        this.connectedStationID = connectedStationID;
    }

}
