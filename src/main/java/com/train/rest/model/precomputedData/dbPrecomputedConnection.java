package com.train.rest.model.precomputedData;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedConnections")
public class dbPrecomputedConnection {

    private int id;
    private dbPrecomputedPart part;
    private Collection<dbPrecomputedStop> precomputedStops;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(targetEntity = dbPrecomputedStop.class ,mappedBy = "dbPrecomputedConnectionID")
    public Collection<dbPrecomputedStop> getPrecomputedStops() {
        return precomputedStops;
    }

    public void setPrecomputedStops(Collection<dbPrecomputedStop> precomputedStops) {
        this.precomputedStops = precomputedStops;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "precomputedPartID")
    @JsonBackReference
    public dbPrecomputedPart getPart() {
        return null;
    }

    public void setPart(dbPrecomputedPart part) {
        this.part = part;
    }



}
