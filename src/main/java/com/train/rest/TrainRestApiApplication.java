package com.train.rest;

import com.train.rest.model.DAO.DbConnectionToHoursDAO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.TimeZone;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = DbConnectionToHoursDAO.class)
@EnableAutoConfiguration
@EntityScan("com.train.rest.model")
public class TrainRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainRestApiApplication.class, args);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}


}
